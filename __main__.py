from .server import serve
import subprocess
import argparse
import os


parser = argparse.ArgumentParser(description="179 s22v IDE")
parser.add_argument("--firefox", dest="firefox", default="firefox", help="Path to Firefox exexutable")
args = parser.parse_args()

subprocess.Popen([
    args.firefox,
    "-profile",
    os.path.join(os.path.dirname(__file__), "profile"),
    "-url",
    "http://127.0.0.1:8080/"
])

serve()