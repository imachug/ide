from .rpc import RPC
from aiohttp import web
import mimetypes
import os
import json


async def handler(request):
    relative_path = request.match_info["path"]
    if relative_path == "":
        relative_path = "index.html"

    try:
        data = open(os.path.dirname(__file__) + "/static/" + relative_path, "rb").read()
    except OSError:
        return web.Response(status=403, content_type="text/html", text="<h1>403 Forbidden</h1>")

    return web.Response(body=data, content_type=mimetypes.guess_type(relative_path)[0])


async def websocketHandler(request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)

    rpc = RPC()

    async for msg in ws:
        data = json.loads(msg.data)
        result = getattr(rpc, data["cmd"])(**data.get("args", {}))
        if result is not None:
            result = {
                "result": result,
                "to": data["id"]
            }
            await ws.send_str(json.dumps(result))

    # Treat disconnection as the end of session
    raise SystemExit(0)


def serve():
    app = web.Application()
    app.add_routes([
        web.get("/websocket", websocketHandler),
        web.get("/{path:.*}", handler)
    ])
    web.run_app(app, port=8080)