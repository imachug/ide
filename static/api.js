window.API = new class API {
	constructor() {
		this.ws = new WebSocket("ws://127.0.0.1:8080/websocket");
		this.ws.onclose = () => {
			// Redirect to make sure there are no timeouts/intervals/whatever.
			// The style is copied from userContent.css
			location.href = "data:text/html;base64," + btoa(`
				<style>
					html {
						display: flex;
						align-items: center;
						justify-content: center;
						height: 100%;
						background-color: #222228;
					}
					html::after {
						max-width: 800px;
						padding: 0 16px;
						content: "Connection to IDE local server is lost. Please close this window and restart the IDE.";
						font-family: "Calibri", "Arial", sans-serif;
						font-size: 24px;
						color: #ffffff;
						text-align: center;
					}
				</style>
			`);
		};
		this.onOpen = new Promise(resolve => this.onOpenResolve = resolve);
		this.ws.onopen = () => {
			this.onOpenResolve();
		}

		this.id = 1;
	}

	async send(request) {
		await this.onOpen;

		return await new Promise(resolve => {
			const id = this.id++;
			request.id = id;
			this.ws.send(JSON.stringify(request));

			const handler = e => {
				const data = JSON.parse(e.data);
				if(data.to === id) {
					this.ws.removeEventListener("onmessage", handler);
					resolve(data.result);
				}
			};
			this.ws.addEventListener("message", handler);
		});
	}


	async getWorkspaces() {
		return await this.send({cmd: "getWorkspaces"})
	}
	async openFile() {
		return await this.send({cmd: "openFile"});
	}
	async getFile(path) {
		return await this.send({cmd: "getFile", args: {path}});
	}
	async saveIDEState(state) {
		return await this.send({cmd: "saveIDEState", args: {state}});
	}
};